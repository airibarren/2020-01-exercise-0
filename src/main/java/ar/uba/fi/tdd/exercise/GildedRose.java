package ar.uba.fi.tdd.exercise;

class  GildedRose {
    private SellableItem[] items;

    //Constructor
    public GildedRose(SellableItem[] _items) {
        items = _items;
    }

    // update the quality of the emements
    public void updateQuality() {
        // for each item
        for (int i = 0; i < items.length; i++) {

            if (!items[i].getName().equals("Aged Brie")
                    && !items[i].getName().equals("Backstage passes to a TAFKAL80ETC concert")) {
                if (items[i].getQuality() > 0)
                    if (!items[i].getName().equals("Sulfuras, Hand of Ragnaros"))
                        items[i].setQuality(items[i].getQuality() - 1);
            } else { // Aged Brie case
                if (items[i].getQuality() < 50) {
                    items[i].setQuality(items[i].getQuality() + 1);

                    if (items[i].getName().equals("Backstage passes to a TAFKAL80ETC concert")) {
                        if (items[i].getSellIn() < 11) {
                            if (items[i].getQuality() < 50) {
                                items[i].setQuality(items[i].getQuality() + 1);
                            }
                        }

                        // si es menor que 6 y si la cantidad es es menor que 50
                        if (items[i].getSellIn() < 6 && items[i].getQuality() < 50) {
                            // sumo 1 a la calidad
                            items[i].setQuality(items[i].getQuality() + 1);
                        }
                    }
            }}

            if (!items[i].getName().equals("Sulfuras, Hand of Ragnaros")) {
                items[i].setSellIn(items[i].getSellIn() - 1);
            }

            if (items[i].getSellIn() < 0) {
                if (!items[i].getName().equals("Aged Brie")) {
                    if (!items[i].getName().equals("Backstage passes to a TAFKAL80ETC concert")) {
                        if (items[i].getQuality() > 0) {
                            if (!items[i].getName().equals("Sulfuras, Hand of Ragnaros")) {
                                items[i].setQuality(items[i].getQuality() - 1);
                            }
                        }
                    } else {
                        items[i].setQuality(items[i].getQuality() - items[i].getQuality());
                    }
                } else {  // Aged Brie case
                    if (items[i].getQuality() < 50) {
                        items[i].setQuality(items[i].getQuality() + 1);
                    }
          }
            }
        }
    }


    //Testing
    public SellableItem getItemInPosition(int i) {
        return items[i];
    }
}

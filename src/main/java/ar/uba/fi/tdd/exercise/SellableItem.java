package ar.uba.fi.tdd.exercise;

public class SellableItem {
    private Item item;

    public SellableItem(Item item) {
        this.item = item;
    }

    public String getName(){
        return this.item.Name;
    }

    public int getQuality(){
        return this.item.quality;
    }

    public void setQuality(int newQuality){
        this.item.quality = newQuality;
    }

    public int getSellIn(){
        return this.item.sellIn;
    }

    public void setSellIn(int newSellIn){
        this.item.sellIn = newSellIn;
    }
}

package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void itemIsCorrectlyAddedToApp() {
		SellableItem testItem = new SellableItem(new Item("Test", 0, 0));
		SellableItem[] items = new SellableItem[]{ testItem };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assert(app.getItemInPosition(0).getName().equals("Test"));
	}

	@Test
	public void qualityIsNeverNegative() {
		SellableItem testItem = new SellableItem(new Item("Test", 0, 0));
		SellableItem[] items = new SellableItem[]{ testItem };
		GildedRose app = new GildedRose(items);
		for (int i=0; i<50; i++){
			app.updateQuality();
		}
		assert(testItem.getQuality() == 0);
	}

	@Test
	public void qualityDegradesTwiceAsFastWhenSellByDateHasPassed() {
		int initialQualityValue = 50, initialSellinValue = 1;
		SellableItem testItem = new SellableItem(new Item("Test", initialSellinValue, initialQualityValue));
		SellableItem[] items = new SellableItem[]{ testItem };
		GildedRose app = new GildedRose(items);
		assert(testItem.getQuality() == initialQualityValue);
		app.updateQuality();
		assert(testItem.getQuality() == initialQualityValue - 1);
		app.updateQuality();
		assert(testItem.getQuality() == initialQualityValue - 3);
		app.updateQuality();
		assert(testItem.getQuality() == initialQualityValue - 5);
	}

	@Test
	public void agedBrieIncreasesQualityTheOlderItGets() {
		int initialQualityValue = 20, initialSellinValue = 1;
		SellableItem agedBrie = new SellableItem(new Item("Aged Brie", initialSellinValue,
															initialQualityValue));
		SellableItem[] items = new SellableItem[]{ agedBrie };
		GildedRose app = new GildedRose(items);
		assertEquals(agedBrie.getQuality(), initialQualityValue);
		app.updateQuality();
		assertTrue(agedBrie.getQuality() > initialQualityValue);
		int actualQuality = agedBrie.getQuality();
		app.updateQuality();
		assertTrue(agedBrie.getQuality() > actualQuality);
		actualQuality = agedBrie.getQuality();
		app.updateQuality();
		assertTrue(agedBrie.getQuality() > actualQuality);
	}

	@Test
	public void qualityCantBeBiggerThan50() {
		int initialQualityValue = 50, initialSellinValue = 1;
		SellableItem agedBrie = new SellableItem(new Item("Aged Brie", initialSellinValue,
				initialQualityValue));
		SellableItem[] items = new SellableItem[]{ agedBrie };
		GildedRose app = new GildedRose(items);
		assertEquals(agedBrie.getQuality(), initialQualityValue);
		app.updateQuality();
		assertEquals(agedBrie.getQuality(), initialQualityValue);
		app.updateQuality();
		assertEquals(agedBrie.getQuality(), initialQualityValue);
		app.updateQuality();
		assertEquals(agedBrie.getQuality(), initialQualityValue);
	}

	@Test
	public void sulfurasNeverHasToBeSoldOrDecreasedInQuality() {
		int initialQualityValue = 50, initialSellinValue = 1;
		SellableItem sulfuras = new SellableItem(new Item("Sulfuras, Hand of Ragnaros", initialSellinValue,
				initialQualityValue));
		SellableItem[] items = new SellableItem[]{ sulfuras };
		GildedRose app = new GildedRose(items);
		assertEquals(sulfuras.getQuality(), initialQualityValue);
		assertEquals(sulfuras.getSellIn(), initialSellinValue);
		app.updateQuality();
		assertEquals(sulfuras.getQuality(), initialQualityValue);
		assertEquals(sulfuras.getSellIn(), initialSellinValue);
		app.updateQuality();
		assertEquals(sulfuras.getQuality(), initialQualityValue);
		assertEquals(sulfuras.getSellIn(), initialSellinValue);
	}

}
